package model;

/**
 * Created by Rosca on 12.04.2017.
 */
public class Orders {
    private int id;
    private int quantity;
    private int client;
    private int product;

    public Orders(){}

    public Orders(int id, int client , int product, int quantity){
        super();
        this.id = id;
        this.client = client;
        this.product = product;
        this.quantity = quantity;
    }
    public Orders( int client , int product, int quantity){
        super();
        this.client = client;
        this.product = product;
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order [client = " + client + " product = " + product + " quantity= " + quantity + " ]\n";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }


    public void setClient(int client) {
        this.client = client;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public int getClient() {
        return client;
    }

    public int getProduct() {
        return product;
    }
}
