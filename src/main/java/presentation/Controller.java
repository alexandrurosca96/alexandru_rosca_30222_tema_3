package presentation;

import bll.ClientBLL;
import bll.OrdersBLL;
import bll.ProductBLL;
import bll.StockBLL;
import model.Client;
import model.Orders;
import model.Product;
import model.Stock;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

/**
 * Created by Rosca on 10.04.2017.
 */
public class Controller {
    private View view;
    private ClientBLL clientBLL;
    private ProductBLL productBLL;
    private StockBLL stockBLL;
    private OrdersBLL ordersBLL;

    int selectValueClients;
    int selectValueProduct;

    public Controller(final View view , final ClientBLL clientBLL, final ProductBLL productBLL, final StockBLL stockBLL , final OrdersBLL ordersBLL){
        this.view = view;
        this.stockBLL = stockBLL;
        this.clientBLL = clientBLL;
        this.productBLL = productBLL;
        this.ordersBLL = ordersBLL;

        this.view.addActionListenerForAddClient(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Client client = new Client(view.getNameAdd(), view.getAddressAdd(), view.getEmailAdd(), view.getAgeAdd());
                    clientBLL.insertClient(client);
                    view.setMessage("New client has been inserted!");
                }catch(Exception e1){
                    view.setMessage(e1.getMessage());
                }
                }
        });

        this.view.addActionListenerForSelectClients(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTable table = clientBLL.createTable(clientBLL.findAllClients());
                JOptionPane.showMessageDialog(null,new JScrollPane(table));
                //get id from selected row in table , if no one in selected id = 1
                try {
                    selectValueClients = (Integer) table.getValueAt(table.getSelectedRow(), 0);
                }catch (Exception e1){
                    selectValueClients = 1;
                }

                Client client = clientBLL.findClientById(selectValueClients);
                view.setNameEdit(client.getName());
                view.setAddressEdit(client.getAddress());
                view.setEmailEdit(client.getEmail());
                view.setAgeEdit(client.getAge());

                view.setLabelClient(client.getName());

                view.setMessage(client.getName() + " has been selected!");
            }
        });

        this.view.addActionListenerForSelectProduct(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JTable table = productBLL.createTable(productBLL.findAllProducts());
                JOptionPane.showMessageDialog(null,new JScrollPane(table));

                try {
                    selectValueProduct = (Integer) table.getValueAt(table.getSelectedRow(), 0);
                }catch (Exception e1){
                    selectValueProduct = 1;
                }
                Product product = productBLL.findProductById(selectValueProduct);

                view.setNameProductEdit(product.getName());
                view.setPriceProductEdit(product.getPrice());

                Stock stock = stockBLL.findByIdProduct(product.getId());

                view.setCantitateProductEdit(stock.getQuantityProduct());

                view.setLabelProduct(product.getName());

                view.setMessage(product.getName() + " has benn selected!");

            }
        });

        this.view.addActionListenerForAddProduct(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Product product = new Product(view.getNameProductAdd(), view.getPriceProductAdd());
                    productBLL.insertProduct(product);

                    List<Product> productList = productBLL.findAllProducts(); //find last id inserted
                    Product product1 = productList.get(productList.size() - 1);
                    Stock stock = new Stock(product1.getId(), view.getCantitateAdd());

                    stockBLL.addNewStock(stock);

                    view.setMessage("Product has been inserted!");
                }
                catch (Exception e3){
                    view.setMessage("Can't insert product");
                }
            }
        });

        view.addActionListenerForEditClient(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Client client = new Client(selectValueClients, view.getNameEdit(), view.getAddressEdit(), view.getEmailEdit(), view.getAgeEdit());
                    clientBLL.updateClient(client);

                    view.setMessage("Client has been modified!");
                }catch(Exception e4){
                    view.setMessage("Can't edit client!");
                }
            }
        });


        view.addActionListeneerForDeleteClient(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    clientBLL.deleteClientById(selectValueClients);
                    view.setMessage("Client has been deleted!");
                }catch(Exception e5){
                    view.setMessage("Can't delete client");
                }
            }
        });

        view.addActionListenerForEditProduct(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Product product = new Product(selectValueProduct,view.getNameProductEdit(),view.getPriceProductEdit());
                    productBLL.updateProduct(product);

                    Stock stock = stockBLL.findByIdProduct(selectValueProduct);//find idStock for current product

                    stock.setQuantityProduct(view.getCantitateProductEdit());//set new value for current product from stock

                    stockBLL.updateStock(stock); //update new value in DB

                    view.setMessage("Product has been modified!");
                }catch(Exception e6){
                    view.setMessage("Can't edit product");
                }
            }
        });


        view.addActionListenerForDeleteProduct(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try{
                    Stock stock = stockBLL.findByIdProduct(selectValueProduct);
                    stockBLL.deleteStockById(stock.getId());
                    System.out.println(stock.toString());

                    productBLL.deleteProductById(selectValueProduct);

                    view.setMessage("Product has been deleted!");
                }catch (Exception e7){
                    view.setMessage(e7.getMessage());
                }
            }
        });

        view.addActionListenerForBuy(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int cantitate = view.getCantitateToBuy();
                    if(cantitate > stockBLL.findByIdProduct(selectValueProduct).getQuantityProduct()){
                        throw new NoSuchElementException("Not enough products in stock");
                    }
                    Client client = clientBLL.findClientById(selectValueClients);
                    Product product = productBLL.findProductById(selectValueProduct);
                    Orders orders = new Orders(product.getId(), client.getId(),cantitate);
                    ordersBLL.addNewOrder(product, client,cantitate);
                    stockBLL.decrementStock(product,cantitate);
                    String message = client.getName() + " has bought " + cantitate + " " + product.getName() + " for " + product.getPrice()*cantitate + " lei";
                    view.setMessage(message);
                    writeFile(client,product,cantitate);

                }catch (NoSuchElementException e2){
                    view.setMessage(e2.getMessage());
                }

                catch (Exception e1){
                    view.setMessage("Insert quantity to buy!");
                }

            }
        });
    }

    private void writeFile(Client client, Product product, int cantitate){
        String FILENAME = "E:\\facultate\\Anu 2-AC\\Sem2\\TP\\Warehouse\\files" + client.getId() + product.getId() + cantitate + ".txt";


        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(FILENAME));
            Date now = new Date();
            String time = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(now);

            String content = time + "\n\n Client: " + client.getName() + " with id: "+ client.getId() + "\n Product: " +product.getName() +
                    "  -- price: " + product.getPrice() + "\n Quantity: " + cantitate + "\n Total: " + cantitate* product.getPrice();

            bw.write(content);

            // no need to close it.
            bw.close();

        } catch (IOException e) {
                e.printStackTrace();
        }
    }
}
