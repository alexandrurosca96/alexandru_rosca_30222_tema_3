package presentation;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * Created by Rosca on 16.04.2017.
 */
public class View {

    private JFrame frame;
    private JTextField textFieldName;
    private JTextField textFieldAdress;
    private JTextField textFieldEmail;
    private JTextField textFieldAge;
    private JTextField textFieldNameProduct;
    private JTextField textFieldPrice;
    private JTextField textFieldCantitate;
    private JTextField textFieldAdressEdit;
    private JTextField textFieldNameEdit;
    private JTextField textFieldEmailEdit;
    private JTextField textFieldAgeEdit;
    private JTextField textFieldNameProductEdit;
    private JTextField textFieldPriceEdit;
    private JTextField textFieldCantitateEdit;
    private JButton btnNewButtonAddClient;
    private JButton btnNewButtonAddProduct;
    private JButton btnNewButtonSelectClients;
    private JButton btnNewButtonSelectProducts;
    private JButton btnNewButtonEditClient;
    private  JButton btnNewButtonDeleteClient;
    private JButton btnNewButtonEditProduct;
    private JButton btnNewButtonDeleteProduct;
    private JTextField textField;
    private JLabel lblNewLabel_14;
    private JLabel lblNewLabel_15;
    private JButton btnBuy;
    private JTextPane textPane;

    public View(){
        frame = new JFrame();
        frame.setBounds(0, 0, 400, 700);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);


        JLabel lblNewLabel = new JLabel("Add Client");
        lblNewLabel.setBounds(69, 11, 80, 14);
        frame.getContentPane().add(lblNewLabel);

        JLabel lblNewLabel_1 = new JLabel("Name:");
        lblNewLabel_1.setBounds(10, 36, 46, 14);
        frame.getContentPane().add(lblNewLabel_1);

        JLabel lblNewLabel_2 = new JLabel("Address:");
        lblNewLabel_2.setBounds(10, 75, 46, 14);
        frame.getContentPane().add(lblNewLabel_2);

        JLabel lblNewLabel_3 = new JLabel("Email:");
        lblNewLabel_3.setBounds(10, 106, 46, 14);
        frame.getContentPane().add(lblNewLabel_3);

        JLabel lblNewLabel_4 = new JLabel("Age:");
        lblNewLabel_4.setBounds(10, 131, 46, 14);
        frame.getContentPane().add(lblNewLabel_4);

        textFieldName = new JTextField();
        textFieldName.setBounds(59, 33, 110, 20);
        frame.getContentPane().add(textFieldName);
        textFieldName.setColumns(10);

        textFieldAdress = new JTextField();
        textFieldAdress.setBounds(59, 72, 110, 20);
        frame.getContentPane().add(textFieldAdress);
        textFieldAdress.setColumns(10);

        textFieldEmail = new JTextField();
        textFieldEmail.setBounds(59, 103, 130, 20);
        frame.getContentPane().add(textFieldEmail);
        textFieldEmail.setColumns(10);

        textFieldAge = new JTextField();
        textFieldAge.setBounds(59, 131, 30, 20);
        frame.getContentPane().add(textFieldAge);
        textFieldAge.setColumns(10);

        btnNewButtonAddClient = new JButton("Add");

        btnNewButtonAddClient.setBounds(59, 156, 79, 23);
        frame.getContentPane().add(btnNewButtonAddClient);

        JLabel lblNewLabel_5 = new JLabel("Add Product");
        lblNewLabel_5.setBounds(252, 11, 80, 14);
        frame.getContentPane().add(lblNewLabel_5);

        JLabel lblNewLabel_6 = new JLabel("Name:");
        lblNewLabel_6.setBounds(193, 36, 46, 14);
        frame.getContentPane().add(lblNewLabel_6);

        textFieldNameProduct = new JTextField();
        textFieldNameProduct.setBounds(252, 33, 110, 20);
        frame.getContentPane().add(textFieldNameProduct);
        textFieldNameProduct.setColumns(10);

        JLabel lblNewLabel_7 = new JLabel("Price");
        lblNewLabel_7.setBounds(193, 75, 46, 14);
        frame.getContentPane().add(lblNewLabel_7);

        textFieldPrice = new JTextField();
        textFieldPrice.setBounds(252, 72, 50, 20);
        frame.getContentPane().add(textFieldPrice);
        textFieldPrice.setColumns(10);

        JLabel lblNewLabel_8 = new JLabel("Cantitate:");
        lblNewLabel_8.setBounds(193, 106, 55, 14);
        frame.getContentPane().add(lblNewLabel_8);

        textFieldCantitate = new JTextField();
        textFieldCantitate.setBounds(252, 103, 30, 20);
        frame.getContentPane().add(textFieldCantitate);
        textFieldCantitate.setColumns(10);

        btnNewButtonAddProduct = new JButton("Add");

        btnNewButtonAddProduct.setBounds(249, 156, 89, 23);
        frame.getContentPane().add(btnNewButtonAddProduct);

        JLabel lblNewLabel_9 = new JLabel("Select client:");
        lblNewLabel_9.setBounds(57, 223, 110, 14);
        frame.getContentPane().add(lblNewLabel_9);

        btnNewButtonSelectClients = new JButton("clients");

        btnNewButtonSelectClients.setBounds(49, 248, 89, 23);
        frame.getContentPane().add(btnNewButtonSelectClients);

        JLabel lblNewLabel_10 = new JLabel("Select products:");
        lblNewLabel_10.setBounds(259, 223, 110, 14);
        frame.getContentPane().add(lblNewLabel_10);

        btnNewButtonSelectProducts = new JButton("products");
        btnNewButtonSelectProducts.setBounds(249, 248, 89, 23);
        frame.getContentPane().add(btnNewButtonSelectProducts);

        JLabel lblName = new JLabel("Name:");
        lblName.setBounds(10, 294, 46, 14);
        frame.getContentPane().add(lblName);

        JLabel lblAddress = new JLabel("Address:");
        lblAddress.setBounds(10, 330, 46, 14);
        frame.getContentPane().add(lblAddress);

        JLabel lblEmail = new JLabel("Email:");
        lblEmail.setBounds(10, 373, 46, 14);
        frame.getContentPane().add(lblEmail);

        JLabel lblAge = new JLabel("Age:");
        lblAge.setBounds(10, 409, 46, 14);
        frame.getContentPane().add(lblAge);

        textFieldAdressEdit = new JTextField();
        textFieldAdressEdit.setBounds(52, 327, 110, 20);
        frame.getContentPane().add(textFieldAdressEdit);
        textFieldAdressEdit.setColumns(10);

        textFieldNameEdit = new JTextField();
        textFieldNameEdit.setBounds(52, 291, 110, 20);
        frame.getContentPane().add(textFieldNameEdit);
        textFieldNameEdit.setColumns(10);

        textFieldEmailEdit = new JTextField();
        textFieldEmailEdit.setBounds(52, 370, 145, 20);
        frame.getContentPane().add(textFieldEmailEdit);
        textFieldEmailEdit.setColumns(10);

        textFieldAgeEdit = new JTextField();
        textFieldAgeEdit.setBounds(52, 406, 30, 20);
        frame.getContentPane().add(textFieldAgeEdit);
        textFieldAgeEdit.setColumns(10);

        JLabel lblNewLabel_11 = new JLabel("Name:");
        lblNewLabel_11.setBounds(203, 294, 46, 14);
        frame.getContentPane().add(lblNewLabel_11);

        JLabel lblNewLabel_12 = new JLabel("Price:");
        lblNewLabel_12.setBounds(202, 330, 46, 14);
        frame.getContentPane().add(lblNewLabel_12);

        JLabel lblNewLabel_13 = new JLabel("Current stock:");
        lblNewLabel_13.setBounds(198, 373, 90, 14);
        frame.getContentPane().add(lblNewLabel_13);

        textFieldNameProductEdit = new JTextField();
        textFieldNameProductEdit.setBounds(252, 291, 110, 20);
        frame.getContentPane().add(textFieldNameProductEdit);
        textFieldNameProductEdit.setColumns(10);

        textFieldPriceEdit = new JTextField();
        textFieldPriceEdit.setBounds(252, 327, 50, 20);
        frame.getContentPane().add(textFieldPriceEdit);
        textFieldPriceEdit.setColumns(10);

        textFieldCantitateEdit = new JTextField();
        textFieldCantitateEdit.setBounds(290, 370, 30, 20);
        frame.getContentPane().add(textFieldCantitateEdit);
        textFieldCantitateEdit.setColumns(10);

        btnNewButtonEditClient = new JButton("Edit");
        btnNewButtonEditClient.setBounds(10, 449, 55, 23);
        frame.getContentPane().add(btnNewButtonEditClient);

        btnNewButtonDeleteClient = new JButton("Delete");
        btnNewButtonDeleteClient.setBounds(69, 449, 80, 23);
        frame.getContentPane().add(btnNewButtonDeleteClient);

        btnNewButtonEditProduct = new JButton("Edit");

        btnNewButtonEditProduct.setBounds(229, 449, 67, 23);
        frame.getContentPane().add(btnNewButtonEditProduct);

        btnNewButtonDeleteProduct = new JButton("Delete");
        btnNewButtonDeleteProduct.setBounds(298, 449, 80, 23);
        frame.getContentPane().add(btnNewButtonDeleteProduct);

        textPane = new JTextPane();
        textPane.setBounds(50, 600, 300, 84);
        frame.getContentPane().add(textPane);

        lblNewLabel_14 = new JLabel("No one ");
        lblNewLabel_14.setBounds(10, 511, 110, 14);
        frame.getContentPane().add(lblNewLabel_14);

        JLabel lblWantToBuy = new JLabel(" wants to buy ");
        lblWantToBuy.setBounds(127, 511, 90, 14);
        frame.getContentPane().add(lblWantToBuy);

        lblNewLabel_15 = new JLabel(" anything!");
        lblNewLabel_15.setBounds(212, 511, 80, 14);
        frame.getContentPane().add(lblNewLabel_15);

        JLabel lblSelectQuantity = new JLabel("Select quantity:");
        lblSelectQuantity.setBounds(10, 536, 110, 14);
        frame.getContentPane().add(lblSelectQuantity);

        textField = new JTextField();
        textField.setBounds(110, 536, 29, 20);
        frame.getContentPane().add(textField);
        textField.setColumns(10);

        btnBuy = new JButton("Buy!");
        btnBuy.setBounds(7, 571, 89, 23);
        frame.getContentPane().add(btnBuy);

        frame.setVisible(true);
    }


    public void addActionListenerForBuy(ActionListener al){
        btnBuy.addActionListener(al);
    }
    public void setLabelClient(String client){
        lblNewLabel_14.setText(client);
    }

    public void setLabelProduct(String product){
        lblNewLabel_15.setText(product);

    }

    public int getCantitateToBuy(){
        return Integer.parseInt(textField.getText());
    }


    public String getNameAdd(){
        return textFieldName.getText();
    }

    public String getAddressAdd(){
        return textFieldAdress.getText();
    }

    public String getEmailAdd(){
        return textFieldEmail.getText();
    }
    public int getAgeAdd(){
        return Integer.parseInt(textFieldAge.getText());
    }

    public String getNameProductAdd(){
        return textFieldNameProduct.getText();
    }

    public int getPriceProductAdd(){
        return Integer.parseInt( textFieldPrice.getText());
    }

    public int getCantitateAdd(){
        return Integer.parseInt( textFieldCantitate.getText());
    }

    public void addActionListenerForAddClient(ActionListener al){
        btnNewButtonAddClient.addActionListener(al);
    }

    public void addActionListenerForAddProduct(ActionListener al){
        btnNewButtonAddProduct.addActionListener(al);
    }

    public void addActionListenerForSelectClients(ActionListener al){
        btnNewButtonSelectClients.addActionListener(al);
    }

    public void addActionListenerForSelectProduct(ActionListener al){
        btnNewButtonSelectProducts.addActionListener(al);
    }

    public void addActionListenerForEditClient(ActionListener al){
        btnNewButtonEditClient.addActionListener(al);
    }

    public void addActionListeneerForDeleteClient(ActionListener al){
        btnNewButtonDeleteClient.addActionListener(al);
    }

    public void addActionListenerForEditProduct(ActionListener al){
        btnNewButtonEditProduct.addActionListener(al);
    }

    public void addActionListenerForDeleteProduct(ActionListener al){
        btnNewButtonDeleteProduct.addActionListener(al);
    }

    public void setMessage(String message){
        textPane.setText(message);
    }

    public String getNameEdit(){
        return textFieldNameEdit.getText();
    }

    public void setNameEdit(String text){
        textFieldNameEdit.setText(text);
    }

    public String getAddressEdit(){
        return textFieldAdressEdit.getText();
    }

    public void setAddressEdit(String text){
        textFieldAdressEdit.setText(text);
    }

    public String getEmailEdit(){
        return textFieldEmailEdit.getText();
    }

    public void setEmailEdit(String text){
        textFieldEmailEdit.setText(text);
    }

    public int getAgeEdit(){
        return Integer.parseInt(textFieldAgeEdit.getText());
    }

    public void setAgeEdit(int age){
        textFieldAgeEdit.setText(Integer.toString(age));
    }


    public String getNameProductEdit(){
        return textFieldNameProductEdit.getText();
    }

    public void setNameProductEdit(String text){
        textFieldNameProductEdit.setText(text);
    }

    public int getPriceProductEdit(){
        return Integer.parseInt(textFieldPriceEdit.getText());
    }

    public void setPriceProductEdit(int price){
        textFieldPriceEdit.setText(Integer.toString(price));
    }

    public int getCantitateProductEdit(){
        return Integer.parseInt(textFieldCantitateEdit.getText());
    }

    public void setCantitateProductEdit(int val){
        textFieldCantitateEdit.setText(Integer.toString(val));
    }
}
