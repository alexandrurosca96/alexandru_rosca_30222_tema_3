package bll;

import bll.validators.Validator;
import dao.OrdersDAO;
import model.Client;
import model.Orders;
import model.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Rosca on 15.04.2017.
 */
public class OrdersBLL {
    private List<Validator<Orders>> validators;
    private OrdersDAO ordersDAO;


    public OrdersBLL() {
        validators = new ArrayList<Validator<Orders>>();
        ordersDAO = new OrdersDAO();
    }

    public List<Orders> findAllOrders(){
        List<Orders> orders;
        orders = ordersDAO.findAll();
        if(orders == null){
            throw new NoSuchElementException("Can't find orders");
        }
        return orders;
    }

    public void addNewOrder(Product product, Client client, int cantitate){
        try{
            ordersDAO.addNewOrder(product, client, cantitate);
        }catch(Exception e){
            throw new NoSuchElementException("Can't add new order");
        }
    }

}
