package bll;

import bll.validators.StockQuantityValidator;
import bll.validators.Validator;
import dao.OrdersDAO;
import dao.StockDAO;
import model.Orders;
import model.Product;
import model.Stock;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Rosca on 15.04.2017.
 */
public class StockBLL {
    private List<Validator<Stock>> validators;
    private StockDAO stockDAO;


    public StockBLL() {
        validators = new ArrayList<Validator<Stock>>();
        validators.add(new StockQuantityValidator());
        stockDAO = new StockDAO();
    }

    public List<Stock> findAllStocks(){
        List<Stock> stocks = new ArrayList<Stock>();
        stocks = stockDAO.findAll();
        if(stocks == null) {
            throw new NoSuchElementException("There are no Stocks!");
        }
        return stocks;
    }

    public void decrementStock(Product product , int value){
        try {
            stockDAO.decrementStock(product, value);
        }catch(Exception e){
            throw new NoSuchElementException("Can't update stock");
        }
    }

    public void updateStock(Stock stock){
        try{
            stockDAO.update(stock, stock.getId());
        }catch(Exception ex2){
            throw new NoSuchElementException("can't update Product");
        }
    }

    public void addNewStock(Stock stock) throws Exception{

        for(Validator<Stock> validator: validators){
            validator.validate(stock);
        }
        stockDAO.insert(stock);

    }

    public Stock findByIdProduct(int id){
        Stock stock;
        stock = stockDAO.findByProductId(id);
        if(stock == null) {
            throw new NoSuchElementException("Can't find by id product!");
        }
        return stock;

    }

    public void deleteStockById(int id){
        try{
            stockDAO.deletebyId(id);
        }catch(Exception e3){
            throw new NoSuchElementException("Can't delete stock!");
        }
    }
}
