package bll.validators;

import model.Stock;

/**
 * Created by Rosca on 21.04.2017.
 */
public class StockQuantityValidator implements Validator<Stock> {

    public void validate(Stock stock) {
        if(stock.getQuantityProduct() < 0){
            throw new IllegalArgumentException("Quantity can't be negative!");
        }
    }
}
