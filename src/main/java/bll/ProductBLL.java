package bll;

import bll.validators.ProductPriceValidator;
import bll.validators.Validator;
import dao.ProductDAO;
import model.Client;
import model.Product;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Rosca on 15.04.2017.
 */
public class ProductBLL {
    private List<Validator<Product>> validators;
    private ProductDAO productDAO;


    public ProductBLL() {
        validators = new ArrayList<Validator<Product>>();
        validators.add(new ProductPriceValidator());
        productDAO = new ProductDAO();
    }

    public List<Product> findAllProducts(){
        List<Product> products = new ArrayList<Product>();
        products = productDAO.findAll();
        if(products == null) {
            throw new NoSuchElementException("There are no Products!");
        }
        return products;
    }

    public Product findProductById(int id) {
        Product product = productDAO.findById(id);
        if (product == null) {
            throw new NoSuchElementException("The product with id =" + id + " was not found!");
        }
        return product;
    }

    public void deleteProductById(int id){
        try{
            productDAO.deletebyId(id);
        }catch(Exception e1){
            throw new NoSuchElementException("Can't delete product!");
        }
    }

    public void updateProduct(Product product){
        try{
            productDAO.update(product, product.getId());
        }catch(Exception ex2){
            throw new NoSuchElementException("can't update Product");
        }
    }

    public void insertProduct(Product product) throws Exception{

        for(Validator<Product> validator: validators){
            validator.validate(product);
        }
        productDAO.insert(product);

    }

    public JTable createTable(List<Product> products){
        JTable table;
        try{
            table = productDAO.createTable(products);
        }catch(Exception ex4){
            throw new NoSuchElementException("Can't create table for Products");
        }
        return table;
    }
}
