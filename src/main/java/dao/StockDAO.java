package dao;

import connection.ConnectionFactory;
import model.Product;
import model.Stock;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Level;

/**
 * Created by Rosca on 15.04.2017.
 */
public class StockDAO extends  AbstractDAO<Stock> {

    public void decrementStock(Product product, int value){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement preparedStatement1 = null;
        ResultSet resultSet = null;
        List<Stock> stock = new ArrayList<Stock>();
        String query = "UPDATE stock SET quantityProduct = ? WHERE product = ?";
        String queryForCurrentStock = "SELECT * FROM stock WHERE product = ?";
        try{
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(queryForCurrentStock);
            preparedStatement.setInt(1,product.getId());
            resultSet = preparedStatement.executeQuery();
            stock = createObjects(resultSet);

            preparedStatement1 = connection.prepareStatement(query);
            preparedStatement1.setInt(1,stock.get(0).getQuantityProduct() - value);
            preparedStatement1.setInt(2, product.getId());

            preparedStatement1.executeUpdate();

        }catch (Exception ex1){
            LOGGER.log(Level.WARNING, "Stock DAO:increment " + ex1.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(preparedStatement1);
            ConnectionFactory.close(connection);
        }
    }


}
