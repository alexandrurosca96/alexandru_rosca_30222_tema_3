package dao;

import connection.ConnectionFactory;
import model.Orders;
import model.Product;
import model.Client;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.NoSuchElementException;
import java.util.logging.Level;

/**
 * Created by Rosca on 15.04.2017.
 */
public class OrdersDAO extends  AbstractDAO<Orders> {

    public void addNewOrder(Product product, Client client, int cantitate){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String query = "INSERT INTO orders (product, client, quantity) VALUES (?, ?, ?) ";

        try{
            connection = ConnectionFactory.getConnection();
            preparedStatement = connection.prepareStatement(query);
            preparedStatement.setInt(1,product.getId());
            preparedStatement.setInt(2, client.getId());
            preparedStatement.setInt(3,cantitate);

            if(preparedStatement.executeUpdate()  == 0 ){
                throw new NoSuchElementException("Can't execute insert");
            }

        }catch(Exception ex){
            LOGGER.log(Level.WARNING, "Orders DAO:Insert " + ex.getMessage());
        }finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(preparedStatement);
            ConnectionFactory.close(connection);
        }

    }
}
