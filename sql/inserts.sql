set foreign_key_checks = 0;

truncate student;
INSERT INTO student(name, address, email, age) VALUES
('Rosca Alexandru', 'Observator 1', 'alex.rosca1996@gmail.com', 21),
('Vasile Ioan', 'Mihai Viteazu 1', 'vasyVasy@yahoo.com', 20);

truncate product;
INSERT INTO product(name, price) VALUES
('Assus 201', 3000),
('Mac Pro', 6000),
('iPhone 7', 2800),
('iPhone 6', 2500),
('HTC 8', 2200);

truncate stock;
INSERT INTO stock(product, quantityProduct) VALUES
(1, 20),
(2, 10),
(3, 7),
(4, 9),
(5, 12);

truncate orders;
INSERT INTO orders(student, product, quantity) VALUES
(1, 2, 2),
(2, 1, 2),
(1, 3, 1);

INSERT INTO student ( name, address, email, age ) VALUES ('Bianca Bucket', 'Obcini nr2', 'biancafloriana07@gmail.com', 21);
SELECT * from student;
